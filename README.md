# Commit Early And Often

Stories that explain why it's helpful to commit sooner rather than later.

![Commit and avoid losing your place!](VersionControl_NowIsAGreatTimeToCommit_Web.jpg)

## Re-use

The source file for these stories are "scalable vector graphics" and are either ".ai" files or ".svg" files.  They can be edited with the (free) program Inkscape, or any vector graphics program.


## Contact

Amy Roberts is currently the maintainer of this repository.  Discussion is welcome; please open an issue to begin a conversation.


## Contribute

Contributions are welcome!  Please open an issue if you are interested in contributing.  Translations and testing are particularly welcome!